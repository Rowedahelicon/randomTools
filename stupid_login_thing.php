<form action="Logout page here" method="POST">
<input name="nothing" value="nothing" type="hidden">
<button type="submit">Logout</button>
</form>

<?php
if($_POST){
	session_start();
	session_destroy();
	if (isset($_COOKIE['whatever'])) {
		setcookie("whatever", "", time() - 3600, "/", "whatever cookie domain you have going on"); //Let's eat a cookie!
	}
	header("Location:wherever);
}else{
	header("Location:ditto as above");
}
?>

The first part is your form bit, the form takes you to whatever logout page you have.
The second bit checks if you've entered this page via POST. If you did, delete your user session and whatever else you need to do.
This stops a problem some sites have where you can literally link / embed the logout page and wipe out a user's session.